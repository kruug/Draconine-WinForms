﻿namespace DesktopWeather
{
    partial class frmCurrentDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCurrentDetails));
            this.btnClose = new System.Windows.Forms.Button();
            this.grpExtra = new System.Windows.Forms.GroupBox();
            this.lblDewPoint = new System.Windows.Forms.Label();
            this.lblHumidity = new System.Windows.Forms.Label();
            this.lblPressure = new System.Windows.Forms.Label();
            this.lblOzone = new System.Windows.Forms.Label();
            this.grpSkyView = new System.Windows.Forms.GroupBox();
            this.lblVisibility = new System.Windows.Forms.Label();
            this.lblCloudCover = new System.Windows.Forms.Label();
            this.grpWind = new System.Windows.Forms.GroupBox();
            this.pbWindBearing = new System.Windows.Forms.PictureBox();
            this.lblWindSpeed = new System.Windows.Forms.Label();
            this.lblWindBearing = new System.Windows.Forms.Label();
            this.grpFeelsLike = new System.Windows.Forms.GroupBox();
            this.lblCurrentFeelsTemp = new System.Windows.Forms.Label();
            this.grpTemperature = new System.Windows.Forms.GroupBox();
            this.lblCurrentTemp = new System.Windows.Forms.Label();
            this.grpPrecipitation = new System.Windows.Forms.GroupBox();
            this.lblPrecipIntensity = new System.Windows.Forms.Label();
            this.lblPrecipProbability = new System.Windows.Forms.Label();
            this.lblPrecipType = new System.Windows.Forms.Label();
            this.grpSummary = new System.Windows.Forms.GroupBox();
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.grpExtra.SuspendLayout();
            this.grpSkyView.SuspendLayout();
            this.grpWind.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWindBearing)).BeginInit();
            this.grpFeelsLike.SuspendLayout();
            this.grpTemperature.SuspendLayout();
            this.grpPrecipitation.SuspendLayout();
            this.grpSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(258, 429);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 50;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grpExtra
            // 
            this.grpExtra.Controls.Add(this.lblDewPoint);
            this.grpExtra.Controls.Add(this.lblHumidity);
            this.grpExtra.Controls.Add(this.lblPressure);
            this.grpExtra.Controls.Add(this.lblOzone);
            this.grpExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpExtra.Location = new System.Drawing.Point(301, 318);
            this.grpExtra.Name = "grpExtra";
            this.grpExtra.Size = new System.Drawing.Size(287, 105);
            this.grpExtra.TabIndex = 47;
            this.grpExtra.TabStop = false;
            this.grpExtra.Text = "Extra Information";
            // 
            // lblDewPoint
            // 
            this.lblDewPoint.AutoSize = true;
            this.lblDewPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDewPoint.Location = new System.Drawing.Point(6, 19);
            this.lblDewPoint.Name = "lblDewPoint";
            this.lblDewPoint.Size = new System.Drawing.Size(89, 20);
            this.lblDewPoint.TabIndex = 22;
            this.lblDewPoint.Text = "Dew Point: ";
            // 
            // lblHumidity
            // 
            this.lblHumidity.AutoSize = true;
            this.lblHumidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHumidity.Location = new System.Drawing.Point(6, 39);
            this.lblHumidity.Name = "lblHumidity";
            this.lblHumidity.Size = new System.Drawing.Size(74, 20);
            this.lblHumidity.TabIndex = 23;
            this.lblHumidity.Text = "Humidity:";
            // 
            // lblPressure
            // 
            this.lblPressure.AutoSize = true;
            this.lblPressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPressure.Location = new System.Drawing.Point(6, 59);
            this.lblPressure.Name = "lblPressure";
            this.lblPressure.Size = new System.Drawing.Size(80, 20);
            this.lblPressure.TabIndex = 24;
            this.lblPressure.Text = "Pressure: ";
            // 
            // lblOzone
            // 
            this.lblOzone.AutoSize = true;
            this.lblOzone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOzone.Location = new System.Drawing.Point(6, 79);
            this.lblOzone.Name = "lblOzone";
            this.lblOzone.Size = new System.Drawing.Size(60, 20);
            this.lblOzone.TabIndex = 25;
            this.lblOzone.Text = "Ozone:";
            // 
            // grpSkyView
            // 
            this.grpSkyView.Controls.Add(this.lblVisibility);
            this.grpSkyView.Controls.Add(this.lblCloudCover);
            this.grpSkyView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSkyView.Location = new System.Drawing.Point(301, 249);
            this.grpSkyView.Name = "grpSkyView";
            this.grpSkyView.Size = new System.Drawing.Size(287, 63);
            this.grpSkyView.TabIndex = 46;
            this.grpSkyView.TabStop = false;
            this.grpSkyView.Text = "Sky View";
            // 
            // lblVisibility
            // 
            this.lblVisibility.AutoSize = true;
            this.lblVisibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisibility.Location = new System.Drawing.Point(6, 19);
            this.lblVisibility.Name = "lblVisibility";
            this.lblVisibility.Size = new System.Drawing.Size(72, 20);
            this.lblVisibility.TabIndex = 20;
            this.lblVisibility.Text = "Visibility: ";
            // 
            // lblCloudCover
            // 
            this.lblCloudCover.AutoSize = true;
            this.lblCloudCover.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloudCover.Location = new System.Drawing.Point(6, 39);
            this.lblCloudCover.Name = "lblCloudCover";
            this.lblCloudCover.Size = new System.Drawing.Size(103, 20);
            this.lblCloudCover.TabIndex = 21;
            this.lblCloudCover.Text = "Cloud Cover: ";
            // 
            // grpWind
            // 
            this.grpWind.Controls.Add(this.pbWindBearing);
            this.grpWind.Controls.Add(this.lblWindSpeed);
            this.grpWind.Controls.Add(this.lblWindBearing);
            this.grpWind.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpWind.Location = new System.Drawing.Point(13, 249);
            this.grpWind.Name = "grpWind";
            this.grpWind.Size = new System.Drawing.Size(287, 100);
            this.grpWind.TabIndex = 45;
            this.grpWind.TabStop = false;
            this.grpWind.Text = "Wind";
            // 
            // pbWindBearing
            // 
            this.pbWindBearing.Location = new System.Drawing.Point(209, 22);
            this.pbWindBearing.Name = "pbWindBearing";
            this.pbWindBearing.Size = new System.Drawing.Size(72, 72);
            this.pbWindBearing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbWindBearing.TabIndex = 20;
            this.pbWindBearing.TabStop = false;
            // 
            // lblWindSpeed
            // 
            this.lblWindSpeed.AutoSize = true;
            this.lblWindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWindSpeed.Location = new System.Drawing.Point(6, 19);
            this.lblWindSpeed.Name = "lblWindSpeed";
            this.lblWindSpeed.Size = new System.Drawing.Size(60, 20);
            this.lblWindSpeed.TabIndex = 18;
            this.lblWindSpeed.Text = "Speed:";
            // 
            // lblWindBearing
            // 
            this.lblWindBearing.AutoSize = true;
            this.lblWindBearing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWindBearing.Location = new System.Drawing.Point(6, 39);
            this.lblWindBearing.Name = "lblWindBearing";
            this.lblWindBearing.Size = new System.Drawing.Size(68, 20);
            this.lblWindBearing.TabIndex = 19;
            this.lblWindBearing.Text = "Bearing:";
            // 
            // grpFeelsLike
            // 
            this.grpFeelsLike.Controls.Add(this.lblCurrentFeelsTemp);
            this.grpFeelsLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFeelsLike.Location = new System.Drawing.Point(301, 207);
            this.grpFeelsLike.Name = "grpFeelsLike";
            this.grpFeelsLike.Size = new System.Drawing.Size(287, 42);
            this.grpFeelsLike.TabIndex = 44;
            this.grpFeelsLike.TabStop = false;
            this.grpFeelsLike.Text = "Apparent Temperature";
            // 
            // lblCurrentFeelsTemp
            // 
            this.lblCurrentFeelsTemp.AutoSize = true;
            this.lblCurrentFeelsTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentFeelsTemp.Location = new System.Drawing.Point(5, 19);
            this.lblCurrentFeelsTemp.Name = "lblCurrentFeelsTemp";
            this.lblCurrentFeelsTemp.Size = new System.Drawing.Size(66, 20);
            this.lblCurrentFeelsTemp.TabIndex = 14;
            this.lblCurrentFeelsTemp.Text = "Current:";
            // 
            // grpTemperature
            // 
            this.grpTemperature.Controls.Add(this.lblCurrentTemp);
            this.grpTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTemperature.Location = new System.Drawing.Point(301, 159);
            this.grpTemperature.Name = "grpTemperature";
            this.grpTemperature.Size = new System.Drawing.Size(287, 42);
            this.grpTemperature.TabIndex = 43;
            this.grpTemperature.TabStop = false;
            this.grpTemperature.Text = "Temperature";
            // 
            // lblCurrentTemp
            // 
            this.lblCurrentTemp.AutoSize = true;
            this.lblCurrentTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentTemp.Location = new System.Drawing.Point(6, 19);
            this.lblCurrentTemp.Name = "lblCurrentTemp";
            this.lblCurrentTemp.Size = new System.Drawing.Size(70, 20);
            this.lblCurrentTemp.TabIndex = 10;
            this.lblCurrentTemp.Text = "Current: ";
            // 
            // grpPrecipitation
            // 
            this.grpPrecipitation.Controls.Add(this.lblPrecipIntensity);
            this.grpPrecipitation.Controls.Add(this.lblPrecipProbability);
            this.grpPrecipitation.Controls.Add(this.lblPrecipType);
            this.grpPrecipitation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPrecipitation.Location = new System.Drawing.Point(13, 159);
            this.grpPrecipitation.Name = "grpPrecipitation";
            this.grpPrecipitation.Size = new System.Drawing.Size(287, 84);
            this.grpPrecipitation.TabIndex = 40;
            this.grpPrecipitation.TabStop = false;
            this.grpPrecipitation.Text = "Precipitation";
            // 
            // lblPrecipIntensity
            // 
            this.lblPrecipIntensity.AutoSize = true;
            this.lblPrecipIntensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipIntensity.Location = new System.Drawing.Point(6, 19);
            this.lblPrecipIntensity.Name = "lblPrecipIntensity";
            this.lblPrecipIntensity.Size = new System.Drawing.Size(77, 20);
            this.lblPrecipIntensity.TabIndex = 2;
            this.lblPrecipIntensity.Text = "Intensity: ";
            // 
            // lblPrecipProbability
            // 
            this.lblPrecipProbability.AutoSize = true;
            this.lblPrecipProbability.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipProbability.Location = new System.Drawing.Point(6, 39);
            this.lblPrecipProbability.Name = "lblPrecipProbability";
            this.lblPrecipProbability.Size = new System.Drawing.Size(85, 20);
            this.lblPrecipProbability.TabIndex = 3;
            this.lblPrecipProbability.Text = "Probability:";
            // 
            // lblPrecipType
            // 
            this.lblPrecipType.AutoSize = true;
            this.lblPrecipType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipType.Location = new System.Drawing.Point(6, 59);
            this.lblPrecipType.Name = "lblPrecipType";
            this.lblPrecipType.Size = new System.Drawing.Size(47, 20);
            this.lblPrecipType.TabIndex = 8;
            this.lblPrecipType.Text = "Type:";
            // 
            // grpSummary
            // 
            this.grpSummary.Controls.Add(this.pbIcon);
            this.grpSummary.Controls.Add(this.lblDate);
            this.grpSummary.Controls.Add(this.lblSummary);
            this.grpSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSummary.Location = new System.Drawing.Point(13, 13);
            this.grpSummary.Name = "grpSummary";
            this.grpSummary.Size = new System.Drawing.Size(575, 140);
            this.grpSummary.TabIndex = 39;
            this.grpSummary.TabStop = false;
            this.grpSummary.Text = "Summary";
            // 
            // pbIcon
            // 
            this.pbIcon.Location = new System.Drawing.Point(6, 66);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(563, 67);
            this.pbIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbIcon.TabIndex = 2;
            this.pbIcon.TabStop = false;
            // 
            // lblDate
            // 
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(3, 19);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(569, 22);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Date";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSummary
            // 
            this.lblSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSummary.Location = new System.Drawing.Point(3, 41);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(569, 22);
            this.lblSummary.TabIndex = 1;
            this.lblSummary.Text = "Summary";
            this.lblSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmCurrentDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 468);
            this.Controls.Add(this.grpFeelsLike);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpExtra);
            this.Controls.Add(this.grpSkyView);
            this.Controls.Add(this.grpWind);
            this.Controls.Add(this.grpTemperature);
            this.Controls.Add(this.grpPrecipitation);
            this.Controls.Add(this.grpSummary);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCurrentDetails";
            this.Text = "frmCurrentDetails";
            this.grpExtra.ResumeLayout(false);
            this.grpExtra.PerformLayout();
            this.grpSkyView.ResumeLayout(false);
            this.grpSkyView.PerformLayout();
            this.grpWind.ResumeLayout(false);
            this.grpWind.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWindBearing)).EndInit();
            this.grpFeelsLike.ResumeLayout(false);
            this.grpFeelsLike.PerformLayout();
            this.grpTemperature.ResumeLayout(false);
            this.grpTemperature.PerformLayout();
            this.grpPrecipitation.ResumeLayout(false);
            this.grpPrecipitation.PerformLayout();
            this.grpSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox grpExtra;
        private System.Windows.Forms.Label lblDewPoint;
        private System.Windows.Forms.Label lblHumidity;
        private System.Windows.Forms.Label lblPressure;
        private System.Windows.Forms.Label lblOzone;
        private System.Windows.Forms.GroupBox grpSkyView;
        private System.Windows.Forms.Label lblVisibility;
        private System.Windows.Forms.Label lblCloudCover;
        private System.Windows.Forms.GroupBox grpWind;
        private System.Windows.Forms.PictureBox pbWindBearing;
        private System.Windows.Forms.Label lblWindSpeed;
        private System.Windows.Forms.Label lblWindBearing;
        private System.Windows.Forms.GroupBox grpFeelsLike;
        private System.Windows.Forms.Label lblCurrentFeelsTemp;
        private System.Windows.Forms.GroupBox grpTemperature;
        private System.Windows.Forms.Label lblCurrentTemp;
        private System.Windows.Forms.GroupBox grpPrecipitation;
        private System.Windows.Forms.Label lblPrecipIntensity;
        private System.Windows.Forms.Label lblPrecipProbability;
        private System.Windows.Forms.Label lblPrecipType;
        private System.Windows.Forms.GroupBox grpSummary;
        private System.Windows.Forms.PictureBox pbIcon;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.Label lblDate;
    }
}