﻿namespace DesktopWeather
{
    partial class frmForecastDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmForecastDetails));
            this.lblDate = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.lblPrecipIntensity = new System.Windows.Forms.Label();
            this.lblPrecipProbability = new System.Windows.Forms.Label();
            this.lblSunrise = new System.Windows.Forms.Label();
            this.lblSunset = new System.Windows.Forms.Label();
            this.lblMoonPhase = new System.Windows.Forms.Label();
            this.lblPrecipIntensityMax = new System.Windows.Forms.Label();
            this.lblPrecipType = new System.Windows.Forms.Label();
            this.lblPrecipAccumulation = new System.Windows.Forms.Label();
            this.lblHigh = new System.Windows.Forms.Label();
            this.lblHighTime = new System.Windows.Forms.Label();
            this.lblLow = new System.Windows.Forms.Label();
            this.lblLowTime = new System.Windows.Forms.Label();
            this.lblFeelsHigh = new System.Windows.Forms.Label();
            this.lblFeelsHighTime = new System.Windows.Forms.Label();
            this.lblFeelsLow = new System.Windows.Forms.Label();
            this.lblFeelsLowTime = new System.Windows.Forms.Label();
            this.lblWindSpeed = new System.Windows.Forms.Label();
            this.lblWindBearing = new System.Windows.Forms.Label();
            this.lblVisibility = new System.Windows.Forms.Label();
            this.lblCloudCover = new System.Windows.Forms.Label();
            this.lblDewPoint = new System.Windows.Forms.Label();
            this.lblHumidity = new System.Windows.Forms.Label();
            this.lblPressure = new System.Windows.Forms.Label();
            this.lblOzone = new System.Windows.Forms.Label();
            this.grpSummary = new System.Windows.Forms.GroupBox();
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.grpPrecipitation = new System.Windows.Forms.GroupBox();
            this.grpSun = new System.Windows.Forms.GroupBox();
            this.pbSunset = new System.Windows.Forms.PictureBox();
            this.pbSunrise = new System.Windows.Forms.PictureBox();
            this.grpMoon = new System.Windows.Forms.GroupBox();
            this.pbMoonPhase = new System.Windows.Forms.PictureBox();
            this.grpTemperature = new System.Windows.Forms.GroupBox();
            this.grpFeelsLike = new System.Windows.Forms.GroupBox();
            this.grpWind = new System.Windows.Forms.GroupBox();
            this.pbWindBearing = new System.Windows.Forms.PictureBox();
            this.grpSkyView = new System.Windows.Forms.GroupBox();
            this.grpExtra = new System.Windows.Forms.GroupBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.pbDraconemsoft = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.grpPrecipitation.SuspendLayout();
            this.grpSun.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSunset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSunrise)).BeginInit();
            this.grpMoon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoonPhase)).BeginInit();
            this.grpTemperature.SuspendLayout();
            this.grpFeelsLike.SuspendLayout();
            this.grpWind.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWindBearing)).BeginInit();
            this.grpSkyView.SuspendLayout();
            this.grpExtra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraconemsoft)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(3, 19);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(569, 22);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Date";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSummary
            // 
            this.lblSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSummary.Location = new System.Drawing.Point(3, 41);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(569, 22);
            this.lblSummary.TabIndex = 1;
            this.lblSummary.Text = "Summary";
            this.lblSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPrecipIntensity
            // 
            this.lblPrecipIntensity.AutoSize = true;
            this.lblPrecipIntensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipIntensity.Location = new System.Drawing.Point(6, 19);
            this.lblPrecipIntensity.Name = "lblPrecipIntensity";
            this.lblPrecipIntensity.Size = new System.Drawing.Size(77, 20);
            this.lblPrecipIntensity.TabIndex = 2;
            this.lblPrecipIntensity.Text = "Intensity: ";
            // 
            // lblPrecipProbability
            // 
            this.lblPrecipProbability.AutoSize = true;
            this.lblPrecipProbability.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipProbability.Location = new System.Drawing.Point(6, 59);
            this.lblPrecipProbability.Name = "lblPrecipProbability";
            this.lblPrecipProbability.Size = new System.Drawing.Size(85, 20);
            this.lblPrecipProbability.TabIndex = 3;
            this.lblPrecipProbability.Text = "Probability:";
            // 
            // lblSunrise
            // 
            this.lblSunrise.AutoSize = true;
            this.lblSunrise.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSunrise.Location = new System.Drawing.Point(57, 31);
            this.lblSunrise.Name = "lblSunrise";
            this.lblSunrise.Size = new System.Drawing.Size(67, 20);
            this.lblSunrise.TabIndex = 4;
            this.lblSunrise.Text = "Sunrise:";
            // 
            // lblSunset
            // 
            this.lblSunset.AutoSize = true;
            this.lblSunset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSunset.Location = new System.Drawing.Point(314, 31);
            this.lblSunset.Name = "lblSunset";
            this.lblSunset.Size = new System.Drawing.Size(64, 20);
            this.lblSunset.TabIndex = 5;
            this.lblSunset.Text = "Sunset:";
            // 
            // lblMoonPhase
            // 
            this.lblMoonPhase.AutoSize = true;
            this.lblMoonPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoonPhase.Location = new System.Drawing.Point(62, 37);
            this.lblMoonPhase.Name = "lblMoonPhase";
            this.lblMoonPhase.Size = new System.Drawing.Size(58, 20);
            this.lblMoonPhase.TabIndex = 6;
            this.lblMoonPhase.Text = "Phase:";
            // 
            // lblPrecipIntensityMax
            // 
            this.lblPrecipIntensityMax.AutoSize = true;
            this.lblPrecipIntensityMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipIntensityMax.Location = new System.Drawing.Point(6, 39);
            this.lblPrecipIntensityMax.Name = "lblPrecipIntensityMax";
            this.lblPrecipIntensityMax.Size = new System.Drawing.Size(106, 20);
            this.lblPrecipIntensityMax.TabIndex = 7;
            this.lblPrecipIntensityMax.Text = "Max Intensity:";
            // 
            // lblPrecipType
            // 
            this.lblPrecipType.AutoSize = true;
            this.lblPrecipType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipType.Location = new System.Drawing.Point(6, 79);
            this.lblPrecipType.Name = "lblPrecipType";
            this.lblPrecipType.Size = new System.Drawing.Size(47, 20);
            this.lblPrecipType.TabIndex = 8;
            this.lblPrecipType.Text = "Type:";
            // 
            // lblPrecipAccumulation
            // 
            this.lblPrecipAccumulation.AutoSize = true;
            this.lblPrecipAccumulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecipAccumulation.Location = new System.Drawing.Point(7, 99);
            this.lblPrecipAccumulation.Name = "lblPrecipAccumulation";
            this.lblPrecipAccumulation.Size = new System.Drawing.Size(109, 20);
            this.lblPrecipAccumulation.TabIndex = 9;
            this.lblPrecipAccumulation.Text = "Accumulation:";
            // 
            // lblHigh
            // 
            this.lblHigh.AutoSize = true;
            this.lblHigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHigh.Location = new System.Drawing.Point(6, 19);
            this.lblHigh.Name = "lblHigh";
            this.lblHigh.Size = new System.Drawing.Size(50, 20);
            this.lblHigh.TabIndex = 10;
            this.lblHigh.Text = "High: ";
            // 
            // lblHighTime
            // 
            this.lblHighTime.AutoSize = true;
            this.lblHighTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHighTime.Location = new System.Drawing.Point(7, 39);
            this.lblHighTime.Name = "lblHighTime";
            this.lblHighTime.Size = new System.Drawing.Size(84, 20);
            this.lblHighTime.TabIndex = 11;
            this.lblHighTime.Text = "High Time:";
            // 
            // lblLow
            // 
            this.lblLow.AutoSize = true;
            this.lblLow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLow.Location = new System.Drawing.Point(8, 59);
            this.lblLow.Name = "lblLow";
            this.lblLow.Size = new System.Drawing.Size(42, 20);
            this.lblLow.TabIndex = 12;
            this.lblLow.Text = "Low:";
            // 
            // lblLowTime
            // 
            this.lblLowTime.AutoSize = true;
            this.lblLowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLowTime.Location = new System.Drawing.Point(9, 79);
            this.lblLowTime.Name = "lblLowTime";
            this.lblLowTime.Size = new System.Drawing.Size(80, 20);
            this.lblLowTime.TabIndex = 13;
            this.lblLowTime.Text = "Low Time:";
            // 
            // lblFeelsHigh
            // 
            this.lblFeelsHigh.AutoSize = true;
            this.lblFeelsHigh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeelsHigh.Location = new System.Drawing.Point(5, 19);
            this.lblFeelsHigh.Name = "lblFeelsHigh";
            this.lblFeelsHigh.Size = new System.Drawing.Size(46, 20);
            this.lblFeelsHigh.TabIndex = 14;
            this.lblFeelsHigh.Text = "High:";
            // 
            // lblFeelsHighTime
            // 
            this.lblFeelsHighTime.AutoSize = true;
            this.lblFeelsHighTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeelsHighTime.Location = new System.Drawing.Point(5, 39);
            this.lblFeelsHighTime.Name = "lblFeelsHighTime";
            this.lblFeelsHighTime.Size = new System.Drawing.Size(84, 20);
            this.lblFeelsHighTime.TabIndex = 15;
            this.lblFeelsHighTime.Text = "High Time:";
            // 
            // lblFeelsLow
            // 
            this.lblFeelsLow.AutoSize = true;
            this.lblFeelsLow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeelsLow.Location = new System.Drawing.Point(5, 58);
            this.lblFeelsLow.Name = "lblFeelsLow";
            this.lblFeelsLow.Size = new System.Drawing.Size(42, 20);
            this.lblFeelsLow.TabIndex = 16;
            this.lblFeelsLow.Text = "Low:";
            // 
            // lblFeelsLowTime
            // 
            this.lblFeelsLowTime.AutoSize = true;
            this.lblFeelsLowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeelsLowTime.Location = new System.Drawing.Point(5, 78);
            this.lblFeelsLowTime.Name = "lblFeelsLowTime";
            this.lblFeelsLowTime.Size = new System.Drawing.Size(80, 20);
            this.lblFeelsLowTime.TabIndex = 17;
            this.lblFeelsLowTime.Text = "Low Time:";
            // 
            // lblWindSpeed
            // 
            this.lblWindSpeed.AutoSize = true;
            this.lblWindSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWindSpeed.Location = new System.Drawing.Point(6, 19);
            this.lblWindSpeed.Name = "lblWindSpeed";
            this.lblWindSpeed.Size = new System.Drawing.Size(60, 20);
            this.lblWindSpeed.TabIndex = 18;
            this.lblWindSpeed.Text = "Speed:";
            // 
            // lblWindBearing
            // 
            this.lblWindBearing.AutoSize = true;
            this.lblWindBearing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWindBearing.Location = new System.Drawing.Point(6, 39);
            this.lblWindBearing.Name = "lblWindBearing";
            this.lblWindBearing.Size = new System.Drawing.Size(68, 20);
            this.lblWindBearing.TabIndex = 19;
            this.lblWindBearing.Text = "Bearing:";
            // 
            // lblVisibility
            // 
            this.lblVisibility.AutoSize = true;
            this.lblVisibility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisibility.Location = new System.Drawing.Point(6, 19);
            this.lblVisibility.Name = "lblVisibility";
            this.lblVisibility.Size = new System.Drawing.Size(72, 20);
            this.lblVisibility.TabIndex = 20;
            this.lblVisibility.Text = "Visibility: ";
            // 
            // lblCloudCover
            // 
            this.lblCloudCover.AutoSize = true;
            this.lblCloudCover.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloudCover.Location = new System.Drawing.Point(6, 39);
            this.lblCloudCover.Name = "lblCloudCover";
            this.lblCloudCover.Size = new System.Drawing.Size(103, 20);
            this.lblCloudCover.TabIndex = 21;
            this.lblCloudCover.Text = "Cloud Cover: ";
            // 
            // lblDewPoint
            // 
            this.lblDewPoint.AutoSize = true;
            this.lblDewPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDewPoint.Location = new System.Drawing.Point(6, 19);
            this.lblDewPoint.Name = "lblDewPoint";
            this.lblDewPoint.Size = new System.Drawing.Size(89, 20);
            this.lblDewPoint.TabIndex = 22;
            this.lblDewPoint.Text = "Dew Point: ";
            // 
            // lblHumidity
            // 
            this.lblHumidity.AutoSize = true;
            this.lblHumidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHumidity.Location = new System.Drawing.Point(6, 39);
            this.lblHumidity.Name = "lblHumidity";
            this.lblHumidity.Size = new System.Drawing.Size(74, 20);
            this.lblHumidity.TabIndex = 23;
            this.lblHumidity.Text = "Humidity:";
            // 
            // lblPressure
            // 
            this.lblPressure.AutoSize = true;
            this.lblPressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPressure.Location = new System.Drawing.Point(6, 59);
            this.lblPressure.Name = "lblPressure";
            this.lblPressure.Size = new System.Drawing.Size(80, 20);
            this.lblPressure.TabIndex = 24;
            this.lblPressure.Text = "Pressure: ";
            // 
            // lblOzone
            // 
            this.lblOzone.AutoSize = true;
            this.lblOzone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOzone.Location = new System.Drawing.Point(6, 79);
            this.lblOzone.Name = "lblOzone";
            this.lblOzone.Size = new System.Drawing.Size(60, 20);
            this.lblOzone.TabIndex = 25;
            this.lblOzone.Text = "Ozone:";
            // 
            // grpSummary
            // 
            this.grpSummary.Controls.Add(this.pbIcon);
            this.grpSummary.Controls.Add(this.lblDate);
            this.grpSummary.Controls.Add(this.lblSummary);
            this.grpSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSummary.Location = new System.Drawing.Point(13, 13);
            this.grpSummary.Name = "grpSummary";
            this.grpSummary.Size = new System.Drawing.Size(575, 140);
            this.grpSummary.TabIndex = 27;
            this.grpSummary.TabStop = false;
            this.grpSummary.Text = "Summary";
            // 
            // pbIcon
            // 
            this.pbIcon.Location = new System.Drawing.Point(6, 66);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(563, 67);
            this.pbIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbIcon.TabIndex = 2;
            this.pbIcon.TabStop = false;
            // 
            // grpPrecipitation
            // 
            this.grpPrecipitation.Controls.Add(this.lblPrecipIntensity);
            this.grpPrecipitation.Controls.Add(this.lblPrecipProbability);
            this.grpPrecipitation.Controls.Add(this.lblPrecipIntensityMax);
            this.grpPrecipitation.Controls.Add(this.lblPrecipType);
            this.grpPrecipitation.Controls.Add(this.lblPrecipAccumulation);
            this.grpPrecipitation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPrecipitation.Location = new System.Drawing.Point(13, 320);
            this.grpPrecipitation.Name = "grpPrecipitation";
            this.grpPrecipitation.Size = new System.Drawing.Size(287, 123);
            this.grpPrecipitation.TabIndex = 28;
            this.grpPrecipitation.TabStop = false;
            this.grpPrecipitation.Text = "Precipitation";
            // 
            // grpSun
            // 
            this.grpSun.Controls.Add(this.pbSunset);
            this.grpSun.Controls.Add(this.pbSunrise);
            this.grpSun.Controls.Add(this.lblSunrise);
            this.grpSun.Controls.Add(this.lblSunset);
            this.grpSun.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSun.Location = new System.Drawing.Point(13, 160);
            this.grpSun.Name = "grpSun";
            this.grpSun.Size = new System.Drawing.Size(575, 70);
            this.grpSun.TabIndex = 29;
            this.grpSun.TabStop = false;
            this.grpSun.Text = "Sun";
            // 
            // pbSunset
            // 
            this.pbSunset.Location = new System.Drawing.Point(263, 17);
            this.pbSunset.Name = "pbSunset";
            this.pbSunset.Size = new System.Drawing.Size(45, 45);
            this.pbSunset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSunset.TabIndex = 7;
            this.pbSunset.TabStop = false;
            // 
            // pbSunrise
            // 
            this.pbSunrise.Location = new System.Drawing.Point(6, 17);
            this.pbSunrise.Name = "pbSunrise";
            this.pbSunrise.Size = new System.Drawing.Size(45, 45);
            this.pbSunrise.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSunrise.TabIndex = 6;
            this.pbSunrise.TabStop = false;
            // 
            // grpMoon
            // 
            this.grpMoon.Controls.Add(this.pbMoonPhase);
            this.grpMoon.Controls.Add(this.lblMoonPhase);
            this.grpMoon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpMoon.Location = new System.Drawing.Point(13, 237);
            this.grpMoon.Name = "grpMoon";
            this.grpMoon.Size = new System.Drawing.Size(287, 77);
            this.grpMoon.TabIndex = 30;
            this.grpMoon.TabStop = false;
            this.grpMoon.Text = "Moon";
            // 
            // pbMoonPhase
            // 
            this.pbMoonPhase.Location = new System.Drawing.Point(6, 22);
            this.pbMoonPhase.Name = "pbMoonPhase";
            this.pbMoonPhase.Size = new System.Drawing.Size(50, 50);
            this.pbMoonPhase.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMoonPhase.TabIndex = 7;
            this.pbMoonPhase.TabStop = false;
            // 
            // grpTemperature
            // 
            this.grpTemperature.Controls.Add(this.lblHigh);
            this.grpTemperature.Controls.Add(this.lblHighTime);
            this.grpTemperature.Controls.Add(this.lblLow);
            this.grpTemperature.Controls.Add(this.lblLowTime);
            this.grpTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTemperature.Location = new System.Drawing.Point(301, 237);
            this.grpTemperature.Name = "grpTemperature";
            this.grpTemperature.Size = new System.Drawing.Size(287, 102);
            this.grpTemperature.TabIndex = 31;
            this.grpTemperature.TabStop = false;
            this.grpTemperature.Text = "Temperature";
            // 
            // grpFeelsLike
            // 
            this.grpFeelsLike.Controls.Add(this.lblFeelsHigh);
            this.grpFeelsLike.Controls.Add(this.lblFeelsHighTime);
            this.grpFeelsLike.Controls.Add(this.lblFeelsLow);
            this.grpFeelsLike.Controls.Add(this.lblFeelsLowTime);
            this.grpFeelsLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFeelsLike.Location = new System.Drawing.Point(301, 345);
            this.grpFeelsLike.Name = "grpFeelsLike";
            this.grpFeelsLike.Size = new System.Drawing.Size(287, 102);
            this.grpFeelsLike.TabIndex = 32;
            this.grpFeelsLike.TabStop = false;
            this.grpFeelsLike.Text = "Apparent Temperature";
            // 
            // grpWind
            // 
            this.grpWind.Controls.Add(this.pbWindBearing);
            this.grpWind.Controls.Add(this.lblWindSpeed);
            this.grpWind.Controls.Add(this.lblWindBearing);
            this.grpWind.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpWind.Location = new System.Drawing.Point(13, 450);
            this.grpWind.Name = "grpWind";
            this.grpWind.Size = new System.Drawing.Size(287, 100);
            this.grpWind.TabIndex = 33;
            this.grpWind.TabStop = false;
            this.grpWind.Text = "Wind";
            // 
            // pbWindBearing
            // 
            this.pbWindBearing.Location = new System.Drawing.Point(209, 22);
            this.pbWindBearing.Name = "pbWindBearing";
            this.pbWindBearing.Size = new System.Drawing.Size(72, 72);
            this.pbWindBearing.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbWindBearing.TabIndex = 20;
            this.pbWindBearing.TabStop = false;
            // 
            // grpSkyView
            // 
            this.grpSkyView.Controls.Add(this.lblVisibility);
            this.grpSkyView.Controls.Add(this.lblCloudCover);
            this.grpSkyView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSkyView.Location = new System.Drawing.Point(301, 454);
            this.grpSkyView.Name = "grpSkyView";
            this.grpSkyView.Size = new System.Drawing.Size(287, 63);
            this.grpSkyView.TabIndex = 34;
            this.grpSkyView.TabStop = false;
            this.grpSkyView.Text = "Sky View";
            // 
            // grpExtra
            // 
            this.grpExtra.Controls.Add(this.lblDewPoint);
            this.grpExtra.Controls.Add(this.lblHumidity);
            this.grpExtra.Controls.Add(this.lblPressure);
            this.grpExtra.Controls.Add(this.lblOzone);
            this.grpExtra.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpExtra.Location = new System.Drawing.Point(301, 524);
            this.grpExtra.Name = "grpExtra";
            this.grpExtra.Size = new System.Drawing.Size(287, 105);
            this.grpExtra.TabIndex = 35;
            this.grpExtra.TabStop = false;
            this.grpExtra.Text = "Extra Information";
            // 
            // pbLogo
            // 
            this.pbLogo.Image = global::DesktopWeather.Properties.Resources.dark_sky_logo;
            this.pbLogo.Location = new System.Drawing.Point(13, 556);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(143, 73);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogo.TabIndex = 36;
            this.pbLogo.TabStop = false;
            // 
            // pbDraconemsoft
            // 
            this.pbDraconemsoft.BackgroundImage = global::DesktopWeather.Properties.Resources.logo_back;
            this.pbDraconemsoft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbDraconemsoft.Image = global::DesktopWeather.Properties.Resources.logo;
            this.pbDraconemsoft.Location = new System.Drawing.Point(157, 556);
            this.pbDraconemsoft.Name = "pbDraconemsoft";
            this.pbDraconemsoft.Size = new System.Drawing.Size(143, 73);
            this.pbDraconemsoft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDraconemsoft.TabIndex = 37;
            this.pbDraconemsoft.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(258, 635);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 38;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmForecastDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 674);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pbDraconemsoft);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.grpExtra);
            this.Controls.Add(this.grpSkyView);
            this.Controls.Add(this.grpWind);
            this.Controls.Add(this.grpFeelsLike);
            this.Controls.Add(this.grpTemperature);
            this.Controls.Add(this.grpMoon);
            this.Controls.Add(this.grpSun);
            this.Controls.Add(this.grpPrecipitation);
            this.Controls.Add(this.grpSummary);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmForecastDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Details for";
            this.grpSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.grpPrecipitation.ResumeLayout(false);
            this.grpPrecipitation.PerformLayout();
            this.grpSun.ResumeLayout(false);
            this.grpSun.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSunset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSunrise)).EndInit();
            this.grpMoon.ResumeLayout(false);
            this.grpMoon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMoonPhase)).EndInit();
            this.grpTemperature.ResumeLayout(false);
            this.grpTemperature.PerformLayout();
            this.grpFeelsLike.ResumeLayout(false);
            this.grpFeelsLike.PerformLayout();
            this.grpWind.ResumeLayout(false);
            this.grpWind.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWindBearing)).EndInit();
            this.grpSkyView.ResumeLayout(false);
            this.grpSkyView.PerformLayout();
            this.grpExtra.ResumeLayout(false);
            this.grpExtra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraconemsoft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.Label lblPrecipIntensity;
        private System.Windows.Forms.Label lblPrecipProbability;
        private System.Windows.Forms.Label lblSunrise;
        private System.Windows.Forms.Label lblSunset;
        private System.Windows.Forms.Label lblMoonPhase;
        private System.Windows.Forms.Label lblPrecipIntensityMax;
        private System.Windows.Forms.Label lblPrecipType;
        private System.Windows.Forms.Label lblPrecipAccumulation;
        private System.Windows.Forms.Label lblHigh;
        private System.Windows.Forms.Label lblHighTime;
        private System.Windows.Forms.Label lblLow;
        private System.Windows.Forms.Label lblLowTime;
        private System.Windows.Forms.Label lblFeelsHigh;
        private System.Windows.Forms.Label lblFeelsHighTime;
        private System.Windows.Forms.Label lblFeelsLow;
        private System.Windows.Forms.Label lblFeelsLowTime;
        private System.Windows.Forms.Label lblWindSpeed;
        private System.Windows.Forms.Label lblWindBearing;
        private System.Windows.Forms.Label lblVisibility;
        private System.Windows.Forms.Label lblCloudCover;
        private System.Windows.Forms.Label lblDewPoint;
        private System.Windows.Forms.Label lblHumidity;
        private System.Windows.Forms.Label lblPressure;
        private System.Windows.Forms.Label lblOzone;
        private System.Windows.Forms.GroupBox grpSummary;
        private System.Windows.Forms.PictureBox pbIcon;
        private System.Windows.Forms.GroupBox grpPrecipitation;
        private System.Windows.Forms.GroupBox grpSun;
        private System.Windows.Forms.PictureBox pbSunset;
        private System.Windows.Forms.PictureBox pbSunrise;
        private System.Windows.Forms.GroupBox grpMoon;
        private System.Windows.Forms.PictureBox pbMoonPhase;
        private System.Windows.Forms.GroupBox grpTemperature;
        private System.Windows.Forms.GroupBox grpFeelsLike;
        private System.Windows.Forms.GroupBox grpWind;
        private System.Windows.Forms.PictureBox pbWindBearing;
        private System.Windows.Forms.GroupBox grpSkyView;
        private System.Windows.Forms.GroupBox grpExtra;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.PictureBox pbDraconemsoft;
        private System.Windows.Forms.Button btnClose;
    }
}