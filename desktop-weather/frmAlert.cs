﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopWeather
{
    public partial class frmAlert : Form
    {
        string alertURL = "";

        public frmAlert()
        {
            InitializeComponent();
        }

        public void showAlerts(clsForecast forecast)
        {
            lblAlertTitle.Text = forecast.getSetAlertTitle;
            lblAlertTime.Text = "Alert time: " + forecast.getSetAlertTime;
            lblAlertExpire.Text = "Alert expires: " + forecast.getSetAlertExpire;

            string formattedDescription = forecast.getSetAlertDescription.Replace("\r\n", Environment.NewLine).Replace("\n", Environment.NewLine).Replace("\r", Environment.NewLine);

            txtDescription.Text = formattedDescription;

            llblAlertURL.Focus();

            alertURL = forecast.getSetAlertURL;
            
            this.Show();
        }

        private void llblAlertURL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Specify that the link was visited.
            llblAlertURL.LinkVisited = true;
            
            // Navigate to a URL.
            System.Diagnostics.Process.Start(alertURL);
        }
    }
}
