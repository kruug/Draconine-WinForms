﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopWeather
{
    public partial class frmForecastDetails : Form
    {
        public frmForecastDetails()
        {
            InitializeComponent();
        }

        public void showDetails(clsForecast forecast)
        {
            pbSunrise.Image = Properties.Resources.sunrise;
            pbSunset.Image = Properties.Resources.sunset;

            this.Text = "Forecast details for " + forecast.getSetLongDate;
            lblDate.Text = forecast.getSetLongDate;
            lblSummary.Text = forecast.getSetSummary;

            while (lblSummary.Width < System.Windows.Forms.TextRenderer.MeasureText(lblSummary.Text, 
                new Font(lblSummary.Font.FontFamily, lblSummary.Font.Size, lblSummary.Font.Style)).Width)
            {
                lblSummary.Font = new Font(lblSummary.Font.FontFamily, lblSummary.Font.Size - 0.5f, lblSummary.Font.Style);
            }

            pbIcon.Image = forecast.getIcon();

            lblSunrise.Text = "Sunrise: " + forecast.getSetSunrise;
            lblSunset.Text = "Sunset: " + forecast.getSetSunset;

            pbMoonPhase.Image = forecast.getMoonPhase();
            lblMoonPhase.Text = "Phase: " + forecast.getSetMoonPhaseString;

            lblPrecipIntensity.Text = "Intensity: " + forecast.getSetPrecipIntensity + " inches/hour";
            lblPrecipIntensityMax.Text = "Max Intensity: " + forecast.getSetPrecipIntensityMax + " inches";
            lblPrecipProbability.Text = "Probability: " + forecast.getSetPrecipProbability + "%";
            lblPrecipType.Text = "Type: " + forecast.getSetPrecipType;
            lblPrecipAccumulation.Text = "Accumulation: " + forecast.getSetPrecipAccumulation + " inches";

            lblHigh.Text = "High: " + forecast.getSetHigh + "\u00B0" + forecast.Units;
            lblHighTime.Text = "High Time: " + forecast.getSetHighTime;
            lblLow.Text = "Low: " + forecast.getSetLow + "\u00B0" + forecast.Units;
            lblLowTime.Text = "Low Time: " + forecast.getSetLowTime;

            lblFeelsHigh.Text = "High: " + forecast.getSetFeelsHigh + "\u00B0" + forecast.Units;
            lblFeelsHighTime.Text = "High Time: " + forecast.getSetFeelsHighTime;
            lblFeelsLow.Text = "Low: " + forecast.getSetFeelsLow + "\u00B0" + forecast.Units;
            lblFeelsLowTime.Text = "Low Time: " + forecast.getSetFeelsLowTime;

            lblWindSpeed.Text = "Speed: " + forecast.getSetWindSpeed + " miles/hour";
            lblWindBearing.Text = "Bearing: " + forecast.getSetWindBearing + "\u00B0";
            pbWindBearing.Image = forecast.getWindBearing();

            if (forecast.getSetVisibility == 11)
            {
                lblVisibility.Text = "Visibility: 10+ miles";

            } else
            {
                lblVisibility.Text = "Visibility: " + forecast.getSetVisibility + " miles";
            }
            lblCloudCover.Text = "Cloud Cover: " + forecast.getSetCloudCover + "%";

            lblDewPoint.Text = "Dew Point: " + forecast.getSetDewPoint + "\u00B0" + forecast.Units;
            lblHumidity.Text = "Humidity: " + forecast.getSetHumidity + "%";
            lblPressure.Text = "Pressure: " + forecast.getSetPressure + " millibars";
            lblOzone.Text = "Ozone: " + forecast.getSetOzone;

            this.Show();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
