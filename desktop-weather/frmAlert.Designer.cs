﻿namespace DesktopWeather
{
    partial class frmAlert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlert));
            this.lblAlertTitle = new System.Windows.Forms.Label();
            this.lblAlertTime = new System.Windows.Forms.Label();
            this.lblAlertExpire = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.llblAlertURL = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblAlertTitle
            // 
            this.lblAlertTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertTitle.Location = new System.Drawing.Point(12, 13);
            this.lblAlertTitle.Name = "lblAlertTitle";
            this.lblAlertTitle.Size = new System.Drawing.Size(560, 23);
            this.lblAlertTitle.TabIndex = 0;
            this.lblAlertTitle.Text = "Alert Title";
            this.lblAlertTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAlertTime
            // 
            this.lblAlertTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertTime.Location = new System.Drawing.Point(12, 36);
            this.lblAlertTime.Name = "lblAlertTime";
            this.lblAlertTime.Size = new System.Drawing.Size(560, 23);
            this.lblAlertTime.TabIndex = 1;
            this.lblAlertTime.Text = "Alert Time:";
            this.lblAlertTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAlertExpire
            // 
            this.lblAlertExpire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertExpire.Location = new System.Drawing.Point(12, 59);
            this.lblAlertExpire.Name = "lblAlertExpire";
            this.lblAlertExpire.Size = new System.Drawing.Size(560, 23);
            this.lblAlertExpire.TabIndex = 2;
            this.lblAlertExpire.Text = "Alert Expires:";
            this.lblAlertExpire.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(12, 85);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(556, 350);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TabStop = false;
            // 
            // llblAlertURL
            // 
            this.llblAlertURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.llblAlertURL.Location = new System.Drawing.Point(12, 438);
            this.llblAlertURL.Name = "llblAlertURL";
            this.llblAlertURL.Size = new System.Drawing.Size(556, 23);
            this.llblAlertURL.TabIndex = 4;
            this.llblAlertURL.TabStop = true;
            this.llblAlertURL.Text = "More Information";
            this.llblAlertURL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.llblAlertURL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblAlertURL_LinkClicked);
            // 
            // frmAlert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 471);
            this.Controls.Add(this.llblAlertURL);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblAlertExpire);
            this.Controls.Add(this.lblAlertTime);
            this.Controls.Add(this.lblAlertTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAlert";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WEATHER ALERT!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAlertTitle;
        private System.Windows.Forms.Label lblAlertTime;
        private System.Windows.Forms.Label lblAlertExpire;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.LinkLabel llblAlertURL;
    }
}