﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopWeather
{
    public partial class frmCurrentDetails : Form
    {
        public frmCurrentDetails()
        {
            InitializeComponent();
        }

        public void showDetails(clsForecast forecast)
        {
            this.Text = "Forecast details for " + forecast.getSetLongDate;
            lblDate.Text = forecast.getSetLongDate;
            lblSummary.Text = forecast.getSetSummary;

            while (lblSummary.Width < System.Windows.Forms.TextRenderer.MeasureText(lblSummary.Text,
                new Font(lblSummary.Font.FontFamily, lblSummary.Font.Size, lblSummary.Font.Style)).Width)
            {
                lblSummary.Font = new Font(lblSummary.Font.FontFamily, lblSummary.Font.Size - 0.5f, lblSummary.Font.Style);
            }

            pbIcon.Image = forecast.getIcon();

            lblPrecipIntensity.Text = "Intensity: " + forecast.getSetPrecipIntensity + " inches/hour";
            lblPrecipProbability.Text = "Probability: " + forecast.getSetPrecipProbability + "%";
            lblPrecipType.Text = "Type: " + forecast.getSetPrecipType;

            lblCurrentTemp.Text = "Temp: " + forecast.getSetTemp + "\u00B0" + forecast.Units;

            lblCurrentFeelsTemp.Text = "Temp: " + forecast.getSetFeelsLike + "\u00B0" + forecast.Units;

            lblWindSpeed.Text = "Speed: " + forecast.getSetWindSpeed + " miles/hour";
            lblWindBearing.Text = "Bearing: " + forecast.getSetWindBearing + "\u00B0";
            pbWindBearing.Image = forecast.getWindBearing();

            if (forecast.getSetVisibility == 11)
            {
                lblVisibility.Text = "Visibility: 10+ miles";

            }
            else
            {
                lblVisibility.Text = "Visibility: " + forecast.getSetVisibility + " miles";
            }
            lblCloudCover.Text = "Cloud Cover: " + forecast.getSetCloudCover + "%";

            lblDewPoint.Text = "Dew Point: " + forecast.getSetDewPoint + "\u00B0" + forecast.Units;
            lblHumidity.Text = "Humidity: " + forecast.getSetHumidity + "%";
            lblPressure.Text = "Pressure: " + forecast.getSetPressure + " millibars";
            lblOzone.Text = "Ozone: " + forecast.getSetOzone;

            this.Show();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
